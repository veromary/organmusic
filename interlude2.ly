\version "2.22.1"

upper = \relative c'' {
  \clef treble
  \key g \major
  \time 4/4

d4 b g4. g8 | a4. b16 c c4 b | r8 e,8 e fis16 g d8 e16 fis g8 a16 b | c4 b b a | << {s4 d4 d4. d8} \\ {r8 b a g fis a g4~} >> | 
<< {c2. b4} \\ {g8 e a g fis d g4~} >> | << {a2 g2~} \\ {g4 fis r8 e d c} >> | << {g'4 fis g2} \\ {b,4 a d2} >> |
}

lower = \relative c {
  \clef bass
  \key g \major
  \time 4/4

g4 g' e4. e8 fis e fis d g4 g, | c4 c'2 b8 g | a fis g g, d'4 d, | r2 d'4 e a, c d8 fis g g, | c a d d, g a b c d4 d, g2 |
}

\score {
  \new PianoStaff \with { instrumentName = "Organ" }
  <<
    \new Staff = "upper" \upper
    \new Staff = "lower" \lower
  >>
  \layout { }
  \midi { }
}

