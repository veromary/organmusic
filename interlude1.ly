\version "2.22.1"

upper = \relative c'' {
  \clef treble
  \key g \major
  \time 4/4

  r8 d b g e'4 d | c8 d16 e d8 c \grace d8 c b r8 d | << {e2 d} \\ {r4 a2 g4~} >> | <<{c2 b} \\ {g4 fis8 d g2~} >> |
<< {a4 d2 c4~} \\ {g4 fis g4. a16 g} >> | << {c8 d b c b4 a} \\ {fis4 g8 a g4 fis } >> | < d g >1
}

lower = \relative c {
  \clef bass
  \key g \major
  \time 4/4

  << {b'2 c4 b} \\ {s4 g2 s4} >> | << a2 \\ {fis4 d} >> g8 a b g | c c, fis a b b, e g | a a, d fis g, a b c |
d8 d, d' c b c16 d e4 | d g, d' d, | g1
}

\score {
  \new PianoStaff \with { instrumentName = "Organ" }
  <<
    \new Staff = "upper" \upper
    \new Staff = "lower" \lower
  >>
  \layout { }
  \midi { }
}

